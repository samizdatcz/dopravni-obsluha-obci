---
title: "Odříznuté od světa: 161 obcí, kam o víkendu nejede vlak ani autobus"
perex: "V Sokolči u Poděbrad žije 932 obyvatel v necelých čtyřech stovkách domů a o víkendu sem nejezdí hromadná doprava. Je to největší ze seznamu podobných obcí, které se Českému rozhlasu podařilo najít. A hledáme dál."
description: "Jak se liší kraje v dostupnosti veřejné dopravy? Podívejte se na výsledky analýzy jízdních řádů."
authors: ["Jan Cibulka", "Marcel Šulek", "Lucie Hochmanová", "Michal Trnka"]
published: "6. října 2016"
coverimg: https://interaktivni.rozhlas.cz/obce-bez-mhd/media/zastavka.jpg
coverimg_note: "Ilustrační foto <a href='https://commons.wikimedia.org/wiki/File:Broumovice,_zast%C3%A1vka.jpg'>Neustupov-Broumovice, okres Benešov. Autor ŠJů</a>"
socialimg: https://interaktivni.rozhlas.cz/obce-bez-mhd/media/zastavka.jpg
url: "obce-bez-mhd"
libraries: [jquery, leaflet, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/
    title: Doleva, či doprava. Podívejte se, jak volí vaše obec
    perex: Český rozhlas připravil aplikaci, kde zjistíte, čím je vaše obec v kraji výjimečná: Které strany zde vedou, a které naopak propadají.
    image: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/media/socimg.jpg
  - link: https://interaktivni.rozhlas.cz/krajske-skolstvi/
    title: Předvolební analýza: Kraje zachraňují učební obory. Žáci o ně nemají zájem
    perex: V krajských volbách se hraje také o podobu středních škol. Největší část krajských rozpočtů směřuje právě do školství. Jaké nástroje mají budoucí hejtmani k prosazování svých slibů?
    image: https://interaktivni.rozhlas.cz/data/krajske-skolstvi/www/media/socimg.jpg
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?
    image: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
---

Lepší dopravní obslužnost slibovaly v předvolebních kampaních prakticky všechny strany, právě kraje jako objednavatelé spojů mají na lokální autobusy a vlaky hlavní vliv. Český rozhlas proto zjišťoval, do kterých vesnic a měst hromadná doprava o víkendu nezajíždí.

V [Sokolči](https://mapy.cz/s/zSZI) na Poděbradsku už prý spojení o víkendu nemají 40 let. "Téměř v každé rodině je auto, lidé se přizpůsobili," vysvětlil Českému rozhlasu starosta Marián Sypajda z ODS. Nedávno ale na žádost místních začal jezdit autobus v pracovní dny dopoledne. "Lidé ráno vyjížděli do Poděbrad k lékaři na nějaké vyšetření a čekali do dvanácti v Poděbradech na poště, na náměstí a v parku, aby se mohli vrátit," dodal Sypajda.

O navýšení dopravních spojů mohou obce žádat kraj, podle mluvčí kraje Michaely Drobné takových žádostí dorazí 30 ročně. Některým kraj vyhoví, u jiných se obcím snaží požadavek rozmluvit. V Sokolči po natáčení Českého rozhlasu zvažují jednání s krajem o zavedení alespoň jednoho víkendové spoje. Nejdříve se ale vedení obce zeptá lidí, jestli by o sobotní nebo nedělní autobusy vůbec měli zájem.

Autobus o víkendu nejezdí ani do [Dešné na Jindřichohradecku](https://mapy.cz/s/17Bab). Obec špatnou dopravní dostupnost kompenzuje mikrobusem, který jezdí po okolních obcích a do [Dačic](https://mapy.cz/s/jBzJ) dvakrát týdně. Zájem o něj ale příliš není: "Když to kdysi začínalo, tak ten autobus jezdil plný, teď dva cestující," řekl Českému rozhlasu starosta Alois Adam (Sdružení pro rozvoj Dešanska). "Mám auto, autobusem skoro nejezdím," naznačuje starousedlík Jindřich Čech, kam se cestující poděli.

Místní si ale na nedostatek spojů na radnici nestěžují. To potvrzuje i krajský radní pro dopravu Antonín Krák z ČSSD: "Pokud se bavíme o obci Dešná, o žádném požadavku do současné doby nevím," řekl Českému rozhlasu. Místní obyvatele trápí spíše špatné silnice, a to nejen lokální, ale i přes hranice do Rakouska.

Nejvíc míst bez víkendové hromadné dopravy je v Jihočeském kraji, funkční víkendovou zastávku nemá v dosahu (méně než 1,5 kilometru vzdušnou čarou) necelých osm procent všech adres. Se sedmi a šesti procenty pak následují Plzeňský a Karlovarský kraj. Dle očekávání je na tom pak nejlépe Praha, kde neobsloužená adresa není žádná.

<aside class="big">
  <div id="graf_kraj" style="height: 400px;"></div>
</aside>


Obce bez víkendových spojů hledal České rozhlas v [datech o jízdních řádech]( http://www.zive.cz/bleskovky/ceske-jizdni-rady-jsou-konecne-open-data-nikdo-se-v-nich-ale-nevyzna/sc-4-a-179546/default.aspx), jejich přehled v čele se zmíněnou [Sokolčí](https://mapy.cz/s/16MZT) najdete v následující mapce. Vzhledem ke stavu jízdních řádů v Česku ale možná některé chybí (viz níže). Pokud v mapě některou obec postrádáte, napište nám na [Facebooku](https://www.facebook.com/datarozhlas) nebo [Twitteru](https://twitter.com/dataRozhlas).

<aside class="big">
  <div id="map_obce" style="height: 650px;"></div>
</aside>

## Boj o jízdní řády

Celostátní databázi jízdních řádů vede ministerstvo dopravy, které ale její správou už před lety pověřilo brněnskou firmu [CHAPS](http://www.chaps.cz/). Ta se donedávna jejich zveřejňování bránila: Společně s vydavatelstvím Mafra totiž provozuje nejznámější vyhledávač spojení [jizdnirady.cz](http://jizdnirady.idnes.cz/vlakyautobusy/spojeni/), který měsíčně dle statistik [NetMonitor](http://www.netmonitor.cz/verejne-vystupy) navštíví přes 1,5 milionu uživatelů, což představuje nezanedbatelné příjmy z reklamy.

Monopol na jízdní řády narušila až společnost Seznam.cz, která po náročném [soudním sporu dosáhla](https://www.novinky.cz/internet-a-pc/373888-seznam-vyhral-u-us-spor-o-pristup-k-jizdnim-radum.html) zveřejnění databáze. Teprve tehdy se ukázalo, v jak špatném stavu evidence jízdních řádů je.

## Kde je ta zastávka? Nevíme

[Vyhláška ministerstva dopravy](http://www.zakonyprolidi.cz/cs/2014-122/zneni-20150901#p3-1-a), která stanovovala, jaké všechny informace o jízdních řádech se uchovávají, překvapivě nepočítala s polohou zastávky. Není tedy jasné, kde většina zastávek leží, chybí přesné určení místa (jako třeba GPS souřadnice). Společnost CHAPS si zastávky sama zaměřila, jde ale o její obchodní tajemství, veřejnost má nárok pouze na [surové informace](ftp://ftp.cisjr.cz/) z databáze ministerstva dopravy. Tady jsou ale zastávky často určené jen názvem, například *Lhota, náves*. O kterou Lhotu se ale jedná, prakticky není možné zjistit.

S určením polohy zastávek proto pomáhali Českému rozhlasu odborníci z portálu [Mapy.cz](https://mapy.cz/zakladni?planovani-trasy&rp=%7B%22criterion%22%3A%22pubt%22%7D), kteří velkou část zastávek rovněž zaměřili. Datoví novináři Rozhlasu pak spočítali, kam o víkendu zajíždí, či nezajíždí autobus.

## Vlakové jízdní řády jen na obrázku

Zveřejnění databáze jízdních řádů se navíc týká jen autobusů, vlakové řády jsou dostupné pouze v podobě zastávkových řádů. A ty není možné automatizovaně zpracovávat. Situaci by měla alespoň trochu napravit [nařízení vlády](https://mapy.cz/zakladni?planovani-trasy&rp=%7B%22criterion%22%3A%22pubt%22%7D) o povinně zveřejňovaných informacích, zatím ale hrozí, že výsledky budou k dispozici až v roce 2018.

Český rozhlas proto při analýze počítal s každou vlakovou zastávkou jako s obslouženou, což však ne vždy bude pravda. Reálně tedy bude neobsloužených obcí více, než zatím v mapce je. Proto pokud najdete další, dejte nám vědět na [Facebooku](https://www.facebook.com/datarozhlas) nebo [Twitteru](https://twitter.com/dataRozhlas), do mapy je přidáme.

_Jsme rádi, že nám pomáháte hledat další obce._

_Neobsloužené obce, které jsme po tipu od čtenářů do mapky přidali: [Lipec](https://mapy.cz/s/17KhY) u Kolína, [Zvírotice](https://mapy.cz/s/17Lfk) na Slapech, [Holany](https://mapy.cz/s/oq3A) na Českolipsku, [Dřevec](https://mapy.cz/s/17ZTi), [Hodyně](https://mapy.cz/s/17ZU6), [Brodeslavy](https://mapy.cz/s/17ZV7), [Bohy](https://mapy.cz/s/17ZWP), [Buček](https://mapy.cz/s/17ZYN), [Lednice](https://mapy.cz/s/1802c), [Dolní Hradiště](https://mapy.cz/s/1800T), [Kočín](https://mapy.cz/s/1801l), [Kopidlo](https://mapy.cz/s/1801D)._

_Obce (ve skutečnosti s hromadnou dopravou o víkendu), které jsme po upozornění čtenářů odebrali: [Šumice](https://mapy.cz/s/17KhC) u Brna, [Nové Sady](https://mapy.cz/s/17Kh0) u Vyškova, [Újezdec](https://mapy.cz/s/180dJ) na Mělnicku_