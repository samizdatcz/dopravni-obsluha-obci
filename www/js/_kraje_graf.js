$(function () {
    $('#graf_kraj').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Kraje ČR'
        },
        colors: ["#de2d26"],
        xAxis: {
            categories: [
                'Jihočeský',
                'Plzeňský',
                'Karlovarský',
                'Středočeský',
                'Vysočina',
                'Královéhradecký',
                'Pardubický',
                'Ústecký',
                'Liberecký',
                'Moravskoslezský',
                'Olomoucký',
                'Zlínský',
                'Jihomoravský',
                'Praha'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Adresní místa v %'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">Kraj {point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Podíl neobsloužených adres',
            data: [7.86, 7.02, 6.18, 5.94, 5.58, 5.11, 5.07, 4.76, 2.88, 2.10, 1.66, 1.51, 1.22, 0]

        }]
    });
});