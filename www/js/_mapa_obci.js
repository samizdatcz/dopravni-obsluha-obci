var obce = {
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "ICOB": "529851", "NAZOB": "Javorník", "POCET_OBYVATEL_1": 123.0, "addrbody_celkem": 69 }, "geometry": { "type": "Point", "coordinates": [ 15.026529649779965, 49.686494652826696 ] } },
{ "type": "Feature", "properties": { "ICOB": "531073", "NAZOB": "Běštín", "POCET_OBYVATEL_1": 317.0, "addrbody_celkem": 218 }, "geometry": { "type": "Point", "coordinates": [ 14.016226642865233, 49.80729754079487 ] } },
{ "type": "Feature", "properties": { "ICOB": "531219", "NAZOB": "Hředle", "POCET_OBYVATEL_1": 368.0, "addrbody_celkem": 210 }, "geometry": { "type": "Point", "coordinates": [ 13.920593793163796, 49.903790987709435 ] } },
{ "type": "Feature", "properties": { "ICOB": "531626", "NAZOB": "Olešná", "POCET_OBYVATEL_1": 407.0, "addrbody_celkem": 178 }, "geometry": { "type": "Point", "coordinates": [ 13.810004952333726, 49.780352285435811 ] } },
{ "type": "Feature", "properties": { "ICOB": "531685", "NAZOB": "Podluhy", "POCET_OBYVATEL_1": 632.0, "addrbody_celkem": 255 }, "geometry": { "type": "Point", "coordinates": [ 13.913516016262122, 49.815615137027073 ] } },
{ "type": "Feature", "properties": { "ICOB": "512991", "NAZOB": "Drnek", "POCET_OBYVATEL_1": 168.0, "addrbody_celkem": 81 }, "geometry": { "type": "Point", "coordinates": [ 13.972396302842274, 50.195637266299599 ] } },
{ "type": "Feature", "properties": { "ICOB": "531367", "NAZOB": "Adamov", "POCET_OBYVATEL_1": 105.0, "addrbody_celkem": 75 }, "geometry": { "type": "Point", "coordinates": [ 15.40891171770266, 49.857816687660382 ] } },
{ "type": "Feature", "properties": { "ICOB": "534277", "NAZOB": "Onomyšl", "POCET_OBYVATEL_1": 294.0, "addrbody_celkem": 160 }, "geometry": { "type": "Point", "coordinates": [ 15.124252612193636, 49.899228497829313 ] } },
{ "type": "Feature", "properties": { "ICOB": "528196", "NAZOB": "Podveky", "POCET_OBYVATEL_1": 208.0, "addrbody_celkem": 157 }, "geometry": { "type": "Point", "coordinates": [ 14.993995149150841, 49.825043801730615 ] } },
{ "type": "Feature", "properties": { "ICOB": "531065", "NAZOB": "Souňov", "POCET_OBYVATEL_1": 133.0, "addrbody_celkem": 64 }, "geometry": { "type": "Point", "coordinates": [ 15.315557291976946, 49.880022027279843 ] } },
{ "type": "Feature", "properties": { "ICOB": "531987", "NAZOB": "Dolní Zimoř", "POCET_OBYVATEL_1": 74.0, "addrbody_celkem": 46 }, "geometry": { "type": "Point", "coordinates": [ 14.501356476223002, 50.42661511299093 ] } },
{ "type": "Feature", "properties": { "ICOB": "531936", "NAZOB": "Vidim", "POCET_OBYVATEL_1": 145.0, "addrbody_celkem": 117 }, "geometry": { "type": "Point", "coordinates": [ 14.525817293261488, 50.467851272891693 ] } },
{ "type": "Feature", "properties": { "ICOB": "536202", "NAZOB": "Ledce", "POCET_OBYVATEL_1": 363.0, "addrbody_celkem": 231 }, "geometry": { "type": "Point", "coordinates": [ 15.081995733229368, 50.355937576559874 ] } },
{ "type": "Feature", "properties": { "ICOB": "536211", "NAZOB": "Lhotky", "POCET_OBYVATEL_1": 151.0, "addrbody_celkem": 74 }, "geometry": { "type": "Point", "coordinates": [ 15.052843711717044, 50.396302602657613 ] } },
{ "type": "Feature", "properties": { "ICOB": "565563", "NAZOB": "Lipník", "POCET_OBYVATEL_1": 311.0, "addrbody_celkem": 126 }, "geometry": { "type": "Point", "coordinates": [ 14.912420468503491, 50.272546352511291 ] } },
{ "type": "Feature", "properties": { "ICOB": "571156", "NAZOB": "Nemyslovice", "POCET_OBYVATEL_1": 148.0, "addrbody_celkem": 66 }, "geometry": { "type": "Point", "coordinates": [ 14.763816577743027, 50.358448507636076 ] } },
{ "type": "Feature", "properties": { "ICOB": "536377", "NAZOB": "Nová Telib", "POCET_OBYVATEL_1": 220.0, "addrbody_celkem": 109 }, "geometry": { "type": "Point", "coordinates": [ 15.032850907735039, 50.391171074434922 ] } },
{ "type": "Feature", "properties": { "ICOB": "571032", "NAZOB": "Pěčice", "POCET_OBYVATEL_1": 170.0, "addrbody_celkem": 120 }, "geometry": { "type": "Point", "coordinates": [ 15.003986692250496, 50.351150463104133 ] } },
{ "type": "Feature", "properties": { "ICOB": "570923", "NAZOB": "Přepeře", "POCET_OBYVATEL_1": 132.0, "addrbody_celkem": 69 }, "geometry": { "type": "Point", "coordinates": [ 15.101879734740988, 50.467625646760709 ] } },
{ "type": "Feature", "properties": { "ICOB": "599573", "NAZOB": "Sezemice", "POCET_OBYVATEL_1": 3604.0, "addrbody_celkem": 68 }, "geometry": { "type": "Point", "coordinates": [ 15.00508452931458, 50.58555100615213 ] } },
{ "type": "Feature", "properties": { "ICOB": "599671", "NAZOB": "Čilec", "POCET_OBYVATEL_1": 226.0, "addrbody_celkem": 109 }, "geometry": { "type": "Point", "coordinates": [ 14.981643486282538, 50.219393733279823 ] } },
{ "type": "Feature", "properties": { "ICOB": "534854", "NAZOB": "Hořany", "POCET_OBYVATEL_1": 139.0, "addrbody_celkem": 73 }, "geometry": { "type": "Point", "coordinates": [ 14.945787217320076, 50.09760891137477 ] } },
{ "type": "Feature", "properties": { "ICOB": "537250", "NAZOB": "Jíkev", "POCET_OBYVATEL_1": 315.0, "addrbody_celkem": 189 }, "geometry": { "type": "Point", "coordinates": [ 15.060011495979346, 50.264365997797796 ] } },
{ "type": "Feature", "properties": { "ICOB": "599581", "NAZOB": "Jiřice", "POCET_OBYVATEL_1": 883.0, "addrbody_celkem": 106 }, "geometry": { "type": "Point", "coordinates": [ 14.836846753461126, 50.252431479110378 ] } },
{ "type": "Feature", "properties": { "ICOB": "599662", "NAZOB": "Oseček", "POCET_OBYVATEL_1": 134.0, "addrbody_celkem": 220 }, "geometry": { "type": "Point", "coordinates": [ 15.148582110512761, 50.101550472801478 ] } },
{ "type": "Feature", "properties": { "ICOB": "537811", "NAZOB": "Sokoleč", "POCET_OBYVATEL_1": 932.0, "addrbody_celkem": 366 }, "geometry": { "type": "Point", "coordinates": [ 15.106915343668893, 50.098418476427234 ] } },
{ "type": "Feature", "properties": { "ICOB": "537942", "NAZOB": "Vestec", "POCET_OBYVATEL_1": 2221.0, "addrbody_celkem": 146 }, "geometry": { "type": "Point", "coordinates": [ 15.146105185502975, 50.23972556844015 ] } },
{ "type": "Feature", "properties": { "ICOB": "537977", "NAZOB": "Vrbová Lhota", "POCET_OBYVATEL_1": 447.0, "addrbody_celkem": 212 }, "geometry": { "type": "Point", "coordinates": [ 15.062644099762975, 50.111953119644859 ] } },
{ "type": "Feature", "properties": { "ICOB": "540170", "NAZOB": "Drevníky", "POCET_OBYVATEL_1": 320.0, "addrbody_celkem": 176 }, "geometry": { "type": "Point", "coordinates": [ 14.274021989798662, 49.720152384459062 ] } },
{ "type": "Feature", "properties": { "ICOB": "540188", "NAZOB": "Drhovy", "POCET_OBYVATEL_1": 253.0, "addrbody_celkem": 99 }, "geometry": { "type": "Point", "coordinates": [ 14.232376574582489, 49.739055804554575 ] } },
{ "type": "Feature", "properties": { "ICOB": "564524", "NAZOB": "Nepomuk", "POCET_OBYVATEL_1": 3804.0, "addrbody_celkem": 126 }, "geometry": { "type": "Point", "coordinates": [ 13.837255335237552, 49.642784619118125 ] } },
{ "type": "Feature", "properties": { "ICOB": "598437", "NAZOB": "Vševily", "POCET_OBYVATEL_1": 121.0, "addrbody_celkem": 84 }, "geometry": { "type": "Point", "coordinates": [ 13.882536739963525, 49.565166478231085 ] } },
{ "type": "Feature", "properties": { "ICOB": "565377", "NAZOB": "Malinová", "POCET_OBYVATEL_1": 79.0, "addrbody_celkem": 45 }, "geometry": { "type": "Point", "coordinates": [ 13.666914203972837, 50.047812539662537 ] } },
{ "type": "Feature", "properties": { "ICOB": "565504", "NAZOB": "Řeřichy", "POCET_OBYVATEL_1": 100.0, "addrbody_celkem": 69 }, "geometry": { "type": "Point", "coordinates": [ 13.585817437663124, 50.079166315912531 ] } },
{ "type": "Feature", "properties": { "ICOB": "565512", "NAZOB": "Václavy", "POCET_OBYVATEL_1": 64.0, "addrbody_celkem": 43 }, "geometry": { "type": "Point", "coordinates": [ 13.594401391197032, 50.063681060522001 ] } },
{ "type": "Feature", "properties": { "ICOB": "598518", "NAZOB": "Žďár", "POCET_OBYVATEL_1": 244.0, "addrbody_celkem": 64 }, "geometry": { "type": "Point", "coordinates": [ 13.459603885273241, 50.058443722772637 ] } },
{ "type": "Feature", "properties": { "ICOB": "535940", "NAZOB": "Mazelov", "POCET_OBYVATEL_1": 212.0, "addrbody_celkem": 102 }, "geometry": { "type": "Point", "coordinates": [ 14.617784335157188, 49.102944865621716 ] } },
{ "type": "Feature", "properties": { "ICOB": "546143", "NAZOB": "Dešná", "POCET_OBYVATEL_1": 627.0, "addrbody_celkem": 273 }, "geometry": { "type": "Point", "coordinates": [ 15.542828359057591, 48.957570881608689 ] } },
{ "type": "Feature", "properties": { "ICOB": "509078", "NAZOB": "Plavsko", "POCET_OBYVATEL_1": 472.0, "addrbody_celkem": 258 }, "geometry": { "type": "Point", "coordinates": [ 14.904023724151209, 49.084588475022862 ] } },
{ "type": "Feature", "properties": { "ICOB": "561045", "NAZOB": "Smržov", "POCET_OBYVATEL_1": 118.0, "addrbody_celkem": 212 }, "geometry": { "type": "Point", "coordinates": [ 14.682376486286579, 49.076049809184653 ] } },
{ "type": "Feature", "properties": { "ICOB": "507652", "NAZOB": "Višňová", "POCET_OBYVATEL_1": 640.0, "addrbody_celkem": 59 }, "geometry": { "type": "Point", "coordinates": [ 14.841231849055065, 49.219000374977938 ] } },
{ "type": "Feature", "properties": { "ICOB": "562629", "NAZOB": "Vydří", "POCET_OBYVATEL_1": 123.0, "addrbody_celkem": 85 }, "geometry": { "type": "Point", "coordinates": [ 14.945627462662744, 49.092423628453638 ] } },
{ "type": "Feature", "properties": { "ICOB": "508683", "NAZOB": "Záblatí", "POCET_OBYVATEL_1": 76.0, "addrbody_celkem": 62 }, "geometry": { "type": "Point", "coordinates": [ 14.68616507136457, 49.107175550728464 ] } },
{ "type": "Feature", "properties": { "ICOB": "562424", "NAZOB": "Županovice", "POCET_OBYVATEL_1": 79.0, "addrbody_celkem": 20 }, "geometry": { "type": "Point", "coordinates": [ 15.50612621753988, 48.957106976888525 ] } },
{ "type": "Feature", "properties": { "ICOB": "549797", "NAZOB": "Přeštěnice", "POCET_OBYVATEL_1": 298.0, "addrbody_celkem": 101 }, "geometry": { "type": "Point", "coordinates": [ 14.42034336593507, 49.479655059484621 ] } },
{ "type": "Feature", "properties": { "ICOB": "550833", "NAZOB": "Bílsko", "POCET_OBYVATEL_1": 201.0, "addrbody_celkem": 106 }, "geometry": { "type": "Point", "coordinates": [ 14.059003406725147, 49.159033796302168 ] } },
{ "type": "Feature", "properties": { "ICOB": "560171", "NAZOB": "Budyně", "POCET_OBYVATEL_1": 43.0, "addrbody_celkem": 28 }, "geometry": { "type": "Point", "coordinates": [ 14.071158393976908, 49.146291089192133 ] } },
{ "type": "Feature", "properties": { "ICOB": "560405", "NAZOB": "Kladruby", "POCET_OBYVATEL_1": 161.0, "addrbody_celkem": 65 }, "geometry": { "type": "Point", "coordinates": [ 13.763452894419569, 49.268701418004369 ] } },
{ "type": "Feature", "properties": { "ICOB": "536342", "NAZOB": "Krajníčko", "POCET_OBYVATEL_1": 91.0, "addrbody_celkem": 62 }, "geometry": { "type": "Point", "coordinates": [ 14.028741621230282, 49.147535946436726 ] } },
{ "type": "Feature", "properties": { "ICOB": "551261", "NAZOB": "Kraselov", "POCET_OBYVATEL_1": 219.0, "addrbody_celkem": 122 }, "geometry": { "type": "Point", "coordinates": [ 13.803943520736659, 49.227823143697684 ] } },
{ "type": "Feature", "properties": { "ICOB": "560391", "NAZOB": "Kuřimany", "POCET_OBYVATEL_1": 26.0, "addrbody_celkem": 21 }, "geometry": { "type": "Point", "coordinates": [ 13.968122755038339, 49.203890412914461 ] } },
{ "type": "Feature", "properties": { "ICOB": "536750", "NAZOB": "Kváskovice", "POCET_OBYVATEL_1": 116.0, "addrbody_celkem": 43 }, "geometry": { "type": "Point", "coordinates": [ 14.003687558143302, 49.188395033262353 ] } },
{ "type": "Feature", "properties": { "ICOB": "536822", "NAZOB": "Lom", "POCET_OBYVATEL_1": 118.0, "addrbody_celkem": 71 }, "geometry": { "type": "Point", "coordinates": [ 13.988983366889434, 49.410441176737152 ] } },
{ "type": "Feature", "properties": { "ICOB": "536415", "NAZOB": "Měkynec", "POCET_OBYVATEL_1": 38.0, "addrbody_celkem": 34 }, "geometry": { "type": "Point", "coordinates": [ 14.02938051098247, 49.158268595154922 ] } },
{ "type": "Feature", "properties": { "ICOB": "551571", "NAZOB": "Paračov", "POCET_OBYVATEL_1": 105.0, "addrbody_celkem": 59 }, "geometry": { "type": "Point", "coordinates": [ 13.994632932210013, 49.201024768986294 ] } },
{ "type": "Feature", "properties": { "ICOB": "598887", "NAZOB": "Pivkovice", "POCET_OBYVATEL_1": 81.0, "addrbody_celkem": 42 }, "geometry": { "type": "Point", "coordinates": [ 14.069623031176132, 49.177482429625748 ] } },
{ "type": "Feature", "properties": { "ICOB": "536423", "NAZOB": "Radějovice", "POCET_OBYVATEL_1": 350.0, "addrbody_celkem": 21 }, "geometry": { "type": "Point", "coordinates": [ 14.026437279906531, 49.186259773191686 ] } },
{ "type": "Feature", "properties": { "ICOB": "536482", "NAZOB": "Zahorčice", "POCET_OBYVATEL_1": 63.0, "addrbody_celkem": 41 }, "geometry": { "type": "Point", "coordinates": [ 13.817508655889176, 49.208646724689274 ] } },
{ "type": "Feature", "properties": { "ICOB": "552097", "NAZOB": "Borkovice", "POCET_OBYVATEL_1": 230.0, "addrbody_celkem": 117 }, "geometry": { "type": "Point", "coordinates": [ 14.643429182308646, 49.207940527597145 ] } },
{ "type": "Feature", "properties": { "ICOB": "563650", "NAZOB": "Dlouhá Lhota", "POCET_OBYVATEL_1": 167.0, "addrbody_celkem": 78 }, "geometry": { "type": "Point", "coordinates": [ 14.788948306209463, 49.352511959338493 ] } },
{ "type": "Feature", "properties": { "ICOB": "563625", "NAZOB": "Haškovcova Lhota", "POCET_OBYVATEL_1": 73.0, "addrbody_celkem": 44 }, "geometry": { "type": "Point", "coordinates": [ 14.466621233197037, 49.332489096566874 ] } },
{ "type": "Feature", "properties": { "ICOB": "599280", "NAZOB": "Komárov", "POCET_OBYVATEL_1": 331.0, "addrbody_celkem": 66 }, "geometry": { "type": "Point", "coordinates": [ 14.594020345830112, 49.250059175136471 ] } },
{ "type": "Feature", "properties": { "ICOB": "553182", "NAZOB": "Sviny", "POCET_OBYVATEL_1": 344.0, "addrbody_celkem": 133 }, "geometry": { "type": "Point", "coordinates": [ 14.635907138771969, 49.187134240925332 ] } },
{ "type": "Feature", "properties": { "ICOB": "566080", "NAZOB": "Čečovice", "POCET_OBYVATEL_1": 97.0, "addrbody_celkem": 41 }, "geometry": { "type": "Point", "coordinates": [ 13.021729344996229, 49.585054737384802 ] } },
{ "type": "Feature", "properties": { "ICOB": "566179", "NAZOB": "Hradiště", "POCET_OBYVATEL_1": 542.0, "addrbody_celkem": 81 }, "geometry": { "type": "Point", "coordinates": [ 13.051716341248049, 49.464758028552744 ] } },
{ "type": "Feature", "properties": { "ICOB": "566144", "NAZOB": "Kanice", "POCET_OBYVATEL_1": 187.0, "addrbody_celkem": 97 }, "geometry": { "type": "Point", "coordinates": [ 13.072693387626517, 49.47500123907885 ] } },
{ "type": "Feature", "properties": { "ICOB": "599174", "NAZOB": "Močerady", "POCET_OBYVATEL_1": 58.0, "addrbody_celkem": 53 }, "geometry": { "type": "Point", "coordinates": [ 13.07427278121606, 49.517404377672882 ] } },
{ "type": "Feature", "properties": { "ICOB": "599166", "NAZOB": "Němčice", "POCET_OBYVATEL_1": 362.0, "addrbody_celkem": 79 }, "geometry": { "type": "Point", "coordinates": [ 13.077494017725975, 49.425066087446972 ] } },
{ "type": "Feature", "properties": { "ICOB": "554405", "NAZOB": "Úsilov", "POCET_OBYVATEL_1": 141.0, "addrbody_celkem": 48 }, "geometry": { "type": "Point", "coordinates": [ 13.12695624405254, 49.39683835169577 ] } },
{ "type": "Feature", "properties": { "ICOB": "541923", "NAZOB": "Břežany", "POCET_OBYVATEL_1": 140.0, "addrbody_celkem": 115 }, "geometry": { "type": "Point", "coordinates": [ 13.617349468500217, 49.34875058586973 ] } },
{ "type": "Feature", "properties": { "ICOB": "578525", "NAZOB": "Frymburk", "POCET_OBYVATEL_1": 107.0, "addrbody_celkem": 70 }, "geometry": { "type": "Point", "coordinates": [ 13.707267335612288, 49.249547854156454 ] } },
{ "type": "Feature", "properties": { "ICOB": "541931", "NAZOB": "Kvášňovice", "POCET_OBYVATEL_1": 121.0, "addrbody_celkem": 99 }, "geometry": { "type": "Point", "coordinates": [ 13.641965718824675, 49.413011612939293 ] } },
{ "type": "Feature", "properties": { "ICOB": "578410", "NAZOB": "Maňovice", "POCET_OBYVATEL_1": 34.0, "addrbody_celkem": 43 }, "geometry": { "type": "Point", "coordinates": [ 13.653831675219749, 49.392952249264333 ] } },
{ "type": "Feature", "properties": { "ICOB": "566055", "NAZOB": "Vřeskovice", "POCET_OBYVATEL_1": 299.0, "addrbody_celkem": 128 }, "geometry": { "type": "Point", "coordinates": [ 13.271100897838382, 49.525429621612517 ] } },
{ "type": "Feature", "properties": { "ICOB": "540668", "NAZOB": "Honezovice", "POCET_OBYVATEL_1": 226.0, "addrbody_celkem": 130 }, "geometry": { "type": "Point", "coordinates": [ 13.062545589651618, 49.63820246186841 ] } },
{ "type": "Feature", "properties": { "ICOB": "540633", "NAZOB": "Kotovice", "POCET_OBYVATEL_1": 307.0, "addrbody_celkem": 105 }, "geometry": { "type": "Point", "coordinates": [ 13.154467245353191, 49.67199902105412 ] } },
{ "type": "Feature", "properties": { "ICOB": "539716", "NAZOB": "Měcholupy", "POCET_OBYVATEL_1": 224.0, "addrbody_celkem": 109 }, "geometry": { "type": "Point", "coordinates": [ 13.532642688504428, 49.519478567676167 ] } },
{ "type": "Feature", "properties": { "ICOB": "558265", "NAZOB": "Ptenín", "POCET_OBYVATEL_1": 199.0, "addrbody_celkem": 126 }, "geometry": { "type": "Point", "coordinates": [ 13.184944115167875, 49.53149126441992 ] } },
{ "type": "Feature", "properties": { "ICOB": "558494", "NAZOB": "Ves Touškov", "POCET_OBYVATEL_1": 332.0, "addrbody_celkem": 128 }, "geometry": { "type": "Point", "coordinates": [ 13.119217550157581, 49.661588513385496 ] } },
{ "type": "Feature", "properties": { "ICOB": "559032", "NAZOB": "Koryta", "POCET_OBYVATEL_1": 81.0, "addrbody_celkem": 145 }, "geometry": { "type": "Point", "coordinates": [ 13.4747443782242, 49.900500198961716 ] } },
{ "type": "Feature", "properties": { "ICOB": "566462", "NAZOB": "Lochousice", "POCET_OBYVATEL_1": 113.0, "addrbody_celkem": 65 }, "geometry": { "type": "Point", "coordinates": [ 13.090436580464713, 49.675088441893514 ] } },
{ "type": "Feature", "properties": { "ICOB": "566543", "NAZOB": "Myslinka", "POCET_OBYVATEL_1": 176.0, "addrbody_celkem": 66 }, "geometry": { "type": "Point", "coordinates": [ 13.21902368186776, 49.74801139622781 ] } },
{ "type": "Feature", "properties": { "ICOB": "541192", "NAZOB": "Nevid", "POCET_OBYVATEL_1": 178.0, "addrbody_celkem": 67 }, "geometry": { "type": "Point", "coordinates": [ 13.605628879408064, 49.685443479441545 ] } },
{ "type": "Feature", "properties": { "ICOB": "566942", "NAZOB": "Raková", "POCET_OBYVATEL_1": 223.0, "addrbody_celkem": 91 }, "geometry": { "type": "Point", "coordinates": [ 13.581937103654353, 49.700318293317125 ] } },
{ "type": "Feature", "properties": { "ICOB": "566845", "NAZOB": "Skomelno", "POCET_OBYVATEL_1": 212.0, "addrbody_celkem": 125 }, "geometry": { "type": "Point", "coordinates": [ 13.642145872162073, 49.847360123378976 ] } },
{ "type": "Feature", "properties": { "ICOB": "560235", "NAZOB": "Veselá", "POCET_OBYVATEL_1": 219.0, "addrbody_celkem": 126 }, "geometry": { "type": "Point", "coordinates": [ 13.602818591370761, 49.695071843590853 ] } },
{ "type": "Feature", "properties": { "ICOB": "578011", "NAZOB": "Chodov", "POCET_OBYVATEL_1": 14110.0, "addrbody_celkem": 68 }, "geometry": { "type": "Point", "coordinates": [ 12.862890652167039, 50.068511101080517 ] } },
{ "type": "Feature", "properties": { "ICOB": "556947", "NAZOB": "Pila", "POCET_OBYVATEL_1": 498.0, "addrbody_celkem": 179 }, "geometry": { "type": "Point", "coordinates": [ 12.925846495463867, 50.179679177513208 ] } },
{ "type": "Feature", "properties": { "ICOB": "565024", "NAZOB": "Klapý", "POCET_OBYVATEL_1": 488.0, "addrbody_celkem": 229 }, "geometry": { "type": "Point", "coordinates": [ 14.006463602129191, 50.431434790624564 ] } },
{ "type": "Feature", "properties": { "ICOB": "546691", "NAZOB": "Lkáň", "POCET_OBYVATEL_1": 177.0, "addrbody_celkem": 87 }, "geometry": { "type": "Point", "coordinates": [ 13.969658064879649, 50.443020469476792 ] } },
{ "type": "Feature", "properties": { "ICOB": "546721", "NAZOB": "Sedlec", "POCET_OBYVATEL_1": 203.0, "addrbody_celkem": 92 }, "geometry": { "type": "Point", "coordinates": [ 14.01628133831082, 50.450061039139563 ] } },
{ "type": "Feature", "properties": { "ICOB": "565873", "NAZOB": "Vlastislav", "POCET_OBYVATEL_1": 173.0, "addrbody_celkem": 88 }, "geometry": { "type": "Point", "coordinates": [ 13.955420827527204, 50.496965215278841 ] } },
{ "type": "Feature", "properties": { "ICOB": "565903", "NAZOB": "Vrbičany", "POCET_OBYVATEL_1": 235.0, "addrbody_celkem": 112 }, "geometry": { "type": "Point", "coordinates": [ 14.084662507958889, 50.462600310653009 ] } },
{ "type": "Feature", "properties": { "ICOB": "566012", "NAZOB": "Blažim", "POCET_OBYVATEL_1": 67.0, "addrbody_celkem": 88 }, "geometry": { "type": "Point", "coordinates": [ 13.628560049594723, 50.406842157931528 ] } },
{ "type": "Feature", "properties": { "ICOB": "566501", "NAZOB": "Nepomyšl", "POCET_OBYVATEL_1": 396.0, "addrbody_celkem": 212 }, "geometry": { "type": "Point", "coordinates": [ 13.313201813581079, 50.218070006357557 ] } },
{ "type": "Feature", "properties": { "ICOB": "566608", "NAZOB": "Podbořanský Rohozec", "POCET_OBYVATEL_1": 133.0, "addrbody_celkem": 72 }, "geometry": { "type": "Point", "coordinates": [ 13.262760045045193, 50.216425329438778 ] } },
{ "type": "Feature", "properties": { "ICOB": "566918", "NAZOB": "Vinařice", "POCET_OBYVATEL_1": 243.0, "addrbody_celkem": 194 }, "geometry": { "type": "Point", "coordinates": [ 13.822746108818041, 50.265656753480783 ] } },
{ "type": "Feature", "properties": { "ICOB": "546909", "NAZOB": "Lukov", "POCET_OBYVATEL_1": 142.0, "addrbody_celkem": 120 }, "geometry": { "type": "Point", "coordinates": [ 13.885397471856969, 50.528122665201273 ] } },
{ "type": "Feature", "properties": { "ICOB": "561959", "NAZOB": "Polevsko", "POCET_OBYVATEL_1": 365.0, "addrbody_celkem": 210 }, "geometry": { "type": "Point", "coordinates": [ 14.532907663335711, 50.786669744464128 ] } },
{ "type": "Feature", "properties": { "ICOB": "553638", "NAZOB": "Tachov", "POCET_OBYVATEL_1": 12595.0, "addrbody_celkem": 66 }, "geometry": { "type": "Point", "coordinates": [ 14.638437196936428, 50.54331275813621 ] } },
{ "type": "Feature", "properties": { "ICOB": "570290", "NAZOB": "Lišice", "POCET_OBYVATEL_1": 159.0, "addrbody_celkem": 129 }, "geometry": { "type": "Point", "coordinates": [ 15.408059240551536, 50.180953899550708 ] } },
{ "type": "Feature", "properties": { "ICOB": "570915", "NAZOB": "Stará Voda", "POCET_OBYVATEL_1": 129.0, "addrbody_celkem": 74 }, "geometry": { "type": "Point", "coordinates": [ 15.530560439401157, 50.150324436355149 ] } },
{ "type": "Feature", "properties": { "ICOB": "571016", "NAZOB": "Šaplava", "POCET_OBYVATEL_1": 121.0, "addrbody_celkem": 63 }, "geometry": { "type": "Point", "coordinates": [ 15.541948545623054, 50.315045197246711 ] } },
{ "type": "Feature", "properties": { "ICOB": "571059", "NAZOB": "Třesovice", "POCET_OBYVATEL_1": 258.0, "addrbody_celkem": 119 }, "geometry": { "type": "Point", "coordinates": [ 15.688874726252847, 50.265081300495538 ] } },
{ "type": "Feature", "properties": { "ICOB": "548901", "NAZOB": "Bílsko u Hořic", "POCET_OBYVATEL_1": 114.0, "addrbody_celkem": 59 }, "geometry": { "type": "Point", "coordinates": [ 15.600494699327514, 50.36939234665067 ] } },
{ "type": "Feature", "properties": { "ICOB": "572918", "NAZOB": "Holovousy", "POCET_OBYVATEL_1": 528.0, "addrbody_celkem": 256 }, "geometry": { "type": "Point", "coordinates": [ 15.577423797060847, 50.375284628103479 ] } },
{ "type": "Feature", "properties": { "ICOB": "549207", "NAZOB": "Nevratice", "POCET_OBYVATEL_1": 161.0, "addrbody_celkem": 92 }, "geometry": { "type": "Point", "coordinates": [ 15.492942620345829, 50.346945668695902 ] } },
{ "type": "Feature", "properties": { "ICOB": "548880", "NAZOB": "Sukorady", "POCET_OBYVATEL_1": 217.0, "addrbody_celkem": 107 }, "geometry": { "type": "Point", "coordinates": [ 15.582698910218038, 50.3254499763692 ] } },
{ "type": "Feature", "properties": { "ICOB": "573795", "NAZOB": "Vršce", "POCET_OBYVATEL_1": 233.0, "addrbody_celkem": 97 }, "geometry": { "type": "Point", "coordinates": [ 15.323044596590167, 50.323853819174531 ] } },
{ "type": "Feature", "properties": { "ICOB": "573841", "NAZOB": "Židovice", "POCET_OBYVATEL_1": 119.0, "addrbody_celkem": 69 }, "geometry": { "type": "Point", "coordinates": [ 15.3204998059594, 50.295370946896341 ] } },
{ "type": "Feature", "properties": { "ICOB": "576387", "NAZOB": "Kostelecké Horky", "POCET_OBYVATEL_1": 140.0, "addrbody_celkem": 62 }, "geometry": { "type": "Point", "coordinates": [ 16.20685951374621, 50.051659391075312 ] } },
{ "type": "Feature", "properties": { "ICOB": "548693", "NAZOB": "Svídnice", "POCET_OBYVATEL_1": 173.0, "addrbody_celkem": 94 }, "geometry": { "type": "Point", "coordinates": [ 16.223280123844575, 50.086055528991174 ] } },
{ "type": "Feature", "properties": { "ICOB": "573949", "NAZOB": "Biskupice", "POCET_OBYVATEL_1": 440.0, "addrbody_celkem": 39 }, "geometry": { "type": "Point", "coordinates": [ 15.512801362886458, 49.872761857098389 ] } },
{ "type": "Feature", "properties": { "ICOB": "571750", "NAZOB": "Lozice", "POCET_OBYVATEL_1": 158.0, "addrbody_celkem": 71 }, "geometry": { "type": "Point", "coordinates": [ 16.023786397393017, 49.916479040892376 ] } },
{ "type": "Feature", "properties": { "ICOB": "574791", "NAZOB": "Brloh", "POCET_OBYVATEL_1": 232.0, "addrbody_celkem": 113 }, "geometry": { "type": "Point", "coordinates": [ 15.557278483428071, 50.000589291264092 ] } },
{ "type": "Feature", "properties": { "ICOB": "574821", "NAZOB": "Bukovina u Přelouče", "POCET_OBYVATEL_1": 71.0, "addrbody_celkem": 44 }, "geometry": { "type": "Point", "coordinates": [ 15.562870437429229, 49.94823956606313 ] } },
{ "type": "Feature", "properties": { "ICOB": "572764", "NAZOB": "Chrtníky", "POCET_OBYVATEL_1": 87.0, "addrbody_celkem": 54 }, "geometry": { "type": "Point", "coordinates": [ 15.604848218067353, 49.981801197072969 ] } },
{ "type": "Feature", "properties": { "ICOB": "575101", "NAZOB": "Jankovice", "POCET_OBYVATEL_1": 312.0, "addrbody_celkem": 244 }, "geometry": { "type": "Point", "coordinates": [ 15.529152707331797, 50.007636869298139 ] } },
{ "type": "Feature", "properties": { "ICOB": "575313", "NAZOB": "Lipoltice", "POCET_OBYVATEL_1": 407.0, "addrbody_celkem": 228 }, "geometry": { "type": "Point", "coordinates": [ 15.568882322617871, 49.987771099921552 ] } },
{ "type": "Feature", "properties": { "ICOB": "575470", "NAZOB": "Poběžovice u Přelouče", "POCET_OBYVATEL_1": 87.0, "addrbody_celkem": 48 }, "geometry": { "type": "Point", "coordinates": [ 15.581884390446875, 49.991003687952976 ] } },
{ "type": "Feature", "properties": { "ICOB": "572853", "NAZOB": "Pravy", "POCET_OBYVATEL_1": 106.0, "addrbody_celkem": 56 }, "geometry": { "type": "Point", "coordinates": [ 15.624148307086811, 50.131781889208554 ] } },
{ "type": "Feature", "properties": { "ICOB": "575666", "NAZOB": "Sopřeč", "POCET_OBYVATEL_1": 278.0, "addrbody_celkem": 114 }, "geometry": { "type": "Point", "coordinates": [ 15.556714465820185, 50.093605860892318 ] } },
{ "type": "Feature", "properties": { "ICOB": "573027", "NAZOB": "Sovolusky", "POCET_OBYVATEL_1": 138.0, "addrbody_celkem": 66 }, "geometry": { "type": "Point", "coordinates": [ 15.537193895978294, 49.969248496632389 ] } },
{ "type": "Feature", "properties": { "ICOB": "575780", "NAZOB": "Svojšice", "POCET_OBYVATEL_1": 257.0, "addrbody_celkem": 182 }, "geometry": { "type": "Point", "coordinates": [ 15.600972437391661, 49.965338793567192 ] } },
{ "type": "Feature", "properties": { "ICOB": "575917", "NAZOB": "Urbanice", "POCET_OBYVATEL_1": 71.0, "addrbody_celkem": 44 }, "geometry": { "type": "Point", "coordinates": [ 15.567004647737622, 49.973945825625798 ] } },
{ "type": "Feature", "properties": { "ICOB": "575984", "NAZOB": "Vlčí Habřina", "POCET_OBYVATEL_1": 313.0, "addrbody_celkem": 122 }, "geometry": { "type": "Point", "coordinates": [ 15.595945786908192, 50.085833194418854 ] } },
{ "type": "Feature", "properties": { "ICOB": "577961", "NAZOB": "Dětřichov", "POCET_OBYVATEL_1": 338.0, "addrbody_celkem": 172 }, "geometry": { "type": "Point", "coordinates": [ 16.53259876188956, 49.799974466866708 ] } },
{ "type": "Feature", "properties": { "ICOB": "572284", "NAZOB": "Hartinkov", "POCET_OBYVATEL_1": 56.0, "addrbody_celkem": 70 }, "geometry": { "type": "Point", "coordinates": [ 16.813933804420618, 49.679106011358392 ] } },
{ "type": "Feature", "properties": { "ICOB": "572292", "NAZOB": "Vrážné", "POCET_OBYVATEL_1": 72.0, "addrbody_celkem": 43 }, "geometry": { "type": "Point", "coordinates": [ 16.778020512174535, 49.675894684442262 ] } },
{ "type": "Feature", "properties": { "ICOB": "578991", "NAZOB": "Vysoká", "POCET_OBYVATEL_1": 203.0, "addrbody_celkem": 104 }, "geometry": { "type": "Point", "coordinates": [ 16.810096020527052, 49.663617800575423 ] } },
{ "type": "Feature", "properties": { "ICOB": "580465", "NAZOB": "Koldín", "POCET_OBYVATEL_1": 362.0, "addrbody_celkem": 133 }, "geometry": { "type": "Point", "coordinates": [ 16.251617376724329, 50.036825008762179 ] } },
{ "type": "Feature", "properties": { "ICOB": "548430", "NAZOB": "Bezděkov", "POCET_OBYVATEL_1": 131.0, "addrbody_celkem": 103 }, "geometry": { "type": "Point", "coordinates": [ 15.730397146958023, 49.732831732501836 ] } },
{ "type": "Feature", "properties": { "ICOB": "548375", "NAZOB": "Čečkovice", "POCET_OBYVATEL_1": 83.0, "addrbody_celkem": 42 }, "geometry": { "type": "Point", "coordinates": [ 15.663964337532406, 49.784609139999752 ] } },
{ "type": "Feature", "properties": { "ICOB": "548413", "NAZOB": "Jilem", "POCET_OBYVATEL_1": 110.0, "addrbody_celkem": 53 }, "geometry": { "type": "Point", "coordinates": [ 15.583365932210935, 49.71337458356566 ] } },
{ "type": "Feature", "properties": { "ICOB": "569313", "NAZOB": "Prosíčka", "POCET_OBYVATEL_1": 133.0, "addrbody_celkem": 68 }, "geometry": { "type": "Point", "coordinates": [ 15.32222111848942, 49.727168975777126 ] } },
{ "type": "Feature", "properties": { "ICOB": "569411", "NAZOB": "Rybníček", "POCET_OBYVATEL_1": 271.0, "addrbody_celkem": 39 }, "geometry": { "type": "Point", "coordinates": [ 15.50903649808062, 49.7693693711172 ] } },
{ "type": "Feature", "properties": { "ICOB": "548499", "NAZOB": "Sloupno", "POCET_OBYVATEL_1": 515.0, "addrbody_celkem": 29 }, "geometry": { "type": "Point", "coordinates": [ 15.749790466977755, 49.736990541286758 ] } },
{ "type": "Feature", "properties": { "ICOB": "569739", "NAZOB": "Vlkanov", "POCET_OBYVATEL_1": 55.0, "addrbody_celkem": 25 }, "geometry": { "type": "Point", "coordinates": [ 15.356737766655071, 49.715624965955243 ] } },
{ "type": "Feature", "properties": { "ICOB": "548626", "NAZOB": "Zvěstovice", "POCET_OBYVATEL_1": 74.0, "addrbody_celkem": 49 }, "geometry": { "type": "Point", "coordinates": [ 15.512588036310975, 49.84792313019058 ] } },
{ "type": "Feature", "properties": { "ICOB": "547735", "NAZOB": "Čejov", "POCET_OBYVATEL_1": 533.0, "addrbody_celkem": 259 }, "geometry": { "type": "Point", "coordinates": [ 15.379248858285912, 49.5656484426746 ] } },
{ "type": "Feature", "properties": { "ICOB": "561762", "NAZOB": "Dobrá Voda u Pacova", "POCET_OBYVATEL_1": 96.0, "addrbody_celkem": 37 }, "geometry": { "type": "Point", "coordinates": [ 15.027706210565382, 49.404275048679253 ] } },
{ "type": "Feature", "properties": { "ICOB": "598721", "NAZOB": "Libkova Voda", "POCET_OBYVATEL_1": 229.0, "addrbody_celkem": 105 }, "geometry": { "type": "Point", "coordinates": [ 15.191009438604166, 49.376490581135897 ] } },
{ "type": "Feature", "properties": { "ICOB": "561100", "NAZOB": "Lidmaň", "POCET_OBYVATEL_1": 277.0, "addrbody_celkem": 201 }, "geometry": { "type": "Point", "coordinates": [ 15.03672251595639, 49.381833490573875 ] } },
{ "type": "Feature", "properties": { "ICOB": "561941", "NAZOB": "Proseč pod Křemešníkem", "POCET_OBYVATEL_1": 93.0, "addrbody_celkem": 117 }, "geometry": { "type": "Point", "coordinates": [ 15.294943439966435, 49.415243405314158 ] } },
{ "type": "Feature", "properties": { "ICOB": "598755", "NAZOB": "Putimov", "POCET_OBYVATEL_1": 265.0, "addrbody_celkem": 93 }, "geometry": { "type": "Point", "coordinates": [ 15.268767761584138, 49.413826635489158 ] } },
{ "type": "Feature", "properties": { "ICOB": "561274", "NAZOB": "Rovná", "POCET_OBYVATEL_1": 58.0, "addrbody_celkem": 41 }, "geometry": { "type": "Point", "coordinates": [ 15.129397576988573, 49.510187095955828 ] } },
{ "type": "Feature", "properties": { "ICOB": "590428", "NAZOB": "Cidlina", "POCET_OBYVATEL_1": 94.0, "addrbody_celkem": 55 }, "geometry": { "type": "Point", "coordinates": [ 15.735539307809669, 49.128191674668756 ] } },
{ "type": "Feature", "properties": { "ICOB": "550477", "NAZOB": "Chotěbudice", "POCET_OBYVATEL_1": 101.0, "addrbody_celkem": 54 }, "geometry": { "type": "Point", "coordinates": [ 15.574000564276831, 49.057982343436215 ] } },
{ "type": "Feature", "properties": { "ICOB": "587605", "NAZOB": "Lomy", "POCET_OBYVATEL_1": 126.0, "addrbody_celkem": 60 }, "geometry": { "type": "Point", "coordinates": [ 15.601085432869869, 49.065513332704491 ] } },
{ "type": "Feature", "properties": { "ICOB": "544957", "NAZOB": "Lovčovice", "POCET_OBYVATEL_1": 59.0, "addrbody_celkem": 21 }, "geometry": { "type": "Point", "coordinates": [ 15.534322756797753, 48.981473265312026 ] } },
{ "type": "Feature", "properties": { "ICOB": "591149", "NAZOB": "Meziříčko", "POCET_OBYVATEL_1": 186.0, "addrbody_celkem": 47 }, "geometry": { "type": "Point", "coordinates": [ 15.660234416369414, 49.109319067808798 ] } },
{ "type": "Feature", "properties": { "ICOB": "591238", "NAZOB": "Nimpšov", "POCET_OBYVATEL_1": 56.0, "addrbody_celkem": 51 }, "geometry": { "type": "Point", "coordinates": [ 15.747240055809449, 49.022621954738923 ] } },
{ "type": "Feature", "properties": { "ICOB": "591424", "NAZOB": "Přeckov", "POCET_OBYVATEL_1": 75.0, "addrbody_celkem": 40 }, "geometry": { "type": "Point", "coordinates": [ 15.916300186602516, 49.275837140715296 ] } },
{ "type": "Feature", "properties": { "ICOB": "550493", "NAZOB": "Radkovice u Budče", "POCET_OBYVATEL_1": 160.0, "addrbody_celkem": 56 }, "geometry": { "type": "Point", "coordinates": [ 15.621744922968935, 49.082489565323797 ] } },
{ "type": "Feature", "properties": { "ICOB": "591823", "NAZOB": "Štěpkov", "POCET_OBYVATEL_1": 112.0, "addrbody_celkem": 44 }, "geometry": { "type": "Point", "coordinates": [ 15.650203433796912, 49.084011630862427 ] } },
{ "type": "Feature", "properties": { "ICOB": "595624", "NAZOB": "Hodíškov", "POCET_OBYVATEL_1": 150.0, "addrbody_celkem": 65 }, "geometry": { "type": "Point", "coordinates": [ 16.038047848790971, 49.503492383540859 ] } },
{ "type": "Feature", "properties": { "ICOB": "554103", "NAZOB": "Řídeč", "POCET_OBYVATEL_1": 199.0, "addrbody_celkem": 104 }, "geometry": { "type": "Point", "coordinates": [ 17.256779371241919, 49.765735189070988 ] } },
{ "type": "Feature", "properties": { "ICOB": "569119", "NAZOB": "Čavisov", "POCET_OBYVATEL_1": 523.0, "addrbody_celkem": 166 }, "geometry": { "type": "Point", "coordinates": [ 18.080697916344011, 49.829484854981686 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Lipec", "POCET_OBYVATEL_1": 191.0, "addrbody_celkem": 90 }, "geometry": { "type": "Point", "coordinates": [ 15.366370, 50.088544 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Zvírotice", "POCET_OBYVATEL_1": 155, "addrbody_celkem": 109 }, "geometry": { "type": "Point", "coordinates": [ 14.365561, 49.674025 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Holany", "POCET_OBYVATEL_1": 515, "addrbody_celkem": null }, "geometry": { "type": "Point", "coordinates": [ 14.487416, 50.622439 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Dřevec", "POCET_OBYVATEL_1": 115, "addrbody_celkem": 57 }, "geometry": { "type": "Point", "coordinates": [ 13.5389142, 49.9710819 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Hodyně", "POCET_OBYVATEL_1": 91, "addrbody_celkem": 40 }, "geometry": { "type": "Point", "coordinates": [ 13.5309386, 49.9611750 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Brodeslavy", "POCET_OBYVATEL_1": 78, "addrbody_celkem": 42 }, "geometry": { "type": "Point", "coordinates": [ 13.5569872, 49.9536700 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Bohy", "POCET_OBYVATEL_1": 128, "addrbody_celkem": null }, "geometry": { "type": "Point", "coordinates": [ 13.5756678, 49.9378967 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Buček", "POCET_OBYVATEL_1": 59, "addrbody_celkem": 25 }, "geometry": { "type": "Point", "coordinates": [ 13.5226414, 49.9527267 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Lednice", "POCET_OBYVATEL_1": 79, "addrbody_celkem": 34 }, "geometry": { "type": "Point", "coordinates": [ 16.8033931, 48.7999189 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Dolní Hradiště", "POCET_OBYVATEL_1": 50, "addrbody_celkem": null }, "geometry": { "type": "Point", "coordinates": [ 13.4962106, 49.9137350 ] } },
{ "type": "Feature", "properties": { "ICOB": null, "NAZOB": "Kočín", "POCET_OBYVATEL_1": 129, "addrbody_celkem": null }, "geometry": { "type": "Point", "coordinates": [ 13.4761378, 49.9303800 ] } },
{ "type": "Feature", "properties": { "ICOB": "559024", "NAZOB": "Kopidlo", "POCET_OBYVATEL_1": 138, "addrbody_celkem": null }, "geometry": { "type": "Point", "coordinates": [ 13.4643850, 49.9431950 ] } }
]
};

var map = L.map('map_obce', {
		center: [49.7428581, 15.3384111],
		minZoom: 8,
		maxZoom: 12,
		zoom: 8
	});

map.scrollWheelZoom.disable();

var background = L.tileLayer('https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png', {
	attribution: 'mapová data © přispěvatelé <a href="http://www.openstreetmap.org">OpenStreetMap</a>,' 
		+ 'obrazový podkres <a href="http://stamen.com/">Stamen</a>, <a href="https://samizdat.cz/">Samizdat'
});

var labels = L.tileLayer('https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png', {
});

background.addTo(map);
labels.addTo(map);

function onEachFeature(feature, layer) {
			var popupContent = "<p>Obec <b>" + feature.properties.NAZOB
					+ "</b>, " + feature.properties.POCET_OBYVATEL_1 + " obyvatel</p>";

			if (feature.properties && feature.properties.popupContent) {
				popupContent += feature.properties.popupContent;
			}

			layer.bindPopup(popupContent);
		}

L.geoJson(obce, {

			style: function (feature) {
				return feature.properties && feature.properties.style;
			},

			onEachFeature: onEachFeature,

			pointToLayer: function (feature, latlng) {
				return L.circleMarker(latlng, {
					radius: 5,
					fillColor: "#de2d26",
					color: "#de2d26",
					weight: 1,
					opacity: 1,
					fillOpacity: 0.5
				});
			}
		}).addTo(map);


map.on('click', function(e){
	console.log(e);
});